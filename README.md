# FDA Supervised Learning

## Description

A data set with information about different iris plants was provided.
The visualization of the data set and the implementation of a regularized logistic regression model were the main parts of this work.

## Team

Myself

## Technology

Python, NumPy, Matplotlib

## My Responsibility

I was responsible for everything.