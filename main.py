import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Task 1
path = "lab_iris_data.csv"

def converter(classname):
  return (0, 1)[classname == "Iris-virginica"]

data = np.loadtxt(path, encoding="utf8", delimiter=",", converters={3: converter})

iris_versicolor_arr = []
iris_virginica_arr = []
for d in data:
  if (d[3] == 0):
    iris_versicolor_arr.append(d)
  if (d[3] == 1):
    iris_virginica_arr.append(d)
iris_versicolor = np.array(iris_versicolor_arr)
iris_virginica = np.array(iris_virginica_arr)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

iris_versicolor_xx = iris_versicolor[:, 0:1]
iris_versicolor_yy = iris_versicolor[:, 1:2]
iris_versicolor_zz = iris_versicolor[:, 2:3]
iris_virginica_xx = iris_virginica[:, 0:1]
iris_virginica_yy = iris_virginica[:, 1:2]
iris_virginica_zz = iris_virginica[:, 2:3]

ax.scatter(iris_versicolor_xx, iris_versicolor_yy, iris_versicolor_zz, c='b', marker='.')
ax.scatter(iris_virginica_xx, iris_virginica_yy, iris_virginica_zz, c='r', marker='.')
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
plt.show()


# Task 2
class LogisticRegression:

  def __init__(self, learn_rate=0.1, num_iter=100, reg_factor=1, batch_size=20, data=None):
    self.learn_rate = learn_rate
    self.num_iter = num_iter
    self.reg_factor = reg_factor
    self.batch_size = batch_size
    self.data = data
    self.theta = np.array([0., 0., 0., 0.])

  def train(self):
    for t in range(self.num_iter):
      eta = self.learn_rate / np.sqrt(t + 1)
      train_data = []
      for k in range(self.batch_size):
        random_index = np.random.randint(0, len(self.data) - 1)
        train_data.append(data[random_index])
      train_data = np.array(train_data)
      train_data = np.hstack([np.ones([train_data.shape[0], 1]), train_data])
      self.theta = self.theta - eta * 1 / self.batch_size * self.gradient(train_data[:, :4], train_data[:, 4])

  def gradient(self, samples: np.array, labels: np.array) -> np.array:
    return np.dot((self.hypothesis(samples) - labels), samples) + self.reg_factor * self.sign()

  def hypothesis(self, samples: np.array) -> float:
    return 1 / (1 + np.exp(np.dot(-samples, self.theta)))

  def sign(self) -> np.array:
    sign_array = np.array(self.theta)
    sign_array[sign_array == 0] = 0
    sign_array[sign_array > 0] = 1
    sign_array[sign_array < 0] = -1
    return sign_array

  def classify(self, samples: np.array) -> np.array:
    return (self.hypothesis(samples) >= 0.5)


# test algorithm
log_reg = LogisticRegression(learn_rate=0.1, num_iter=100, reg_factor=1, batch_size=20, data=data)
log_reg.train()
data_with_one_column = np.hstack([np.ones([data.shape[0], 1]), data])
classified = log_reg.classify(data_with_one_column[:, :4])
accuracy = (classified == data_with_one_column[:, 4]).mean()
print(log_reg.theta)
print(classified)
print(accuracy)

# Task 3
iris_versicolor_arr = []
iris_virginica_arr = []
for d in data:
  if (d[3] == 0):
    iris_versicolor_arr.append(d)
  if (d[3] == 1):
    iris_virginica_arr.append(d)
iris_versicolor = np.array(iris_versicolor_arr)
iris_virginica = np.array(iris_virginica_arr)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

iris_versicolor_xx = iris_versicolor[:, 0:1]
iris_versicolor_yy = iris_versicolor[:, 1:2]
iris_versicolor_zz = iris_versicolor[:, 2:3]
iris_virginica_xx = iris_virginica[:, 0:1]
iris_virginica_yy = iris_virginica[:, 1:2]
iris_virginica_zz = iris_virginica[:, 2:3]

a = log_reg.theta[1]
b = log_reg.theta[2]
c = log_reg.theta[3]
d = log_reg.theta[0]
x = np.linspace(min(data[:, 0]), max(data[:, 0]), 20)
y = np.linspace(min(data[:, 1]), max(data[:, 1]), 20)
X, Y = np.meshgrid(x, y)
Z = (d - a * X - b * Y) / c
ax.plot_surface(X, Y, Z)

ax.scatter(iris_versicolor_xx, iris_versicolor_yy, iris_versicolor_zz, c='b', marker='.')
ax.scatter(iris_virginica_xx, iris_virginica_yy, iris_virginica_zz, c='r', marker='.')
ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')
plt.show()
